//noinspection JSUnresolvedVariable
module.exports = {
    attributes: {
        season: 'integer',
        league: 'integer',
        isVisitor: 'integer',
        team: {
            model: 'teams'
        },
        score: 'integer',
        points: 'integer',
        gameId: {
            model: 'games'
        }
    }
};
