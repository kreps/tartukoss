module.exports = {
    attributes: {
        season: 'int',
        league: {model: 'leagues'},
        place: {
            model: "places"
        }
    }
}