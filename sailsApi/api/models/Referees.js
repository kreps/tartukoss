/**
 * Referees.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    firstname: 'string',
    lastname: 'string',
    level: 'string',
    phone: 'int',
    email: 'email',
    region: 'string'
  }
};

