module.exports = {

    attributes: {
        name: "string",
        abbr: "string",
        address: "string",
        link: "string"
    }
};