//noinspection JSUnresolvedVariable
module.exports = {
    attributes: {
        datetime: 'datetime',
        home: {model: 'teams'},
        visitor:  {model: 'teams'},
        placeId: {
            model: 'places'
        },
        referee1: 'integer',
        referee2: 'integer',
        referee3: 'integer',
        attendants: 'integer',
        league: 'integer',
        season: 'integer',
        stage: {
            model: 'stages'
        },
        round: 'integer',
        gameresults: {
            collection: 'gameresults',
            via: 'gameId'
        }
    }
};
