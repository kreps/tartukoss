module.exports = {
    attributes: {
        season: 'int',
        player: {
            model: 'players'
        },
        team: {
            model: 'teams'
        },
        league: 'int'
    }
};
