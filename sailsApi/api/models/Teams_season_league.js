module.exports = {
    attributes: {
        idpk:{
            type: 'integer',
            columnName: 'id'
        },
        season: "integer",
        id: {
            type: "integer",
            columnName: 'team'
        },
        league: "integer",
        name: 'string',
        abbr: 'string'
    }
};