module.exports = {
    attributes: {
        id: {
            type: 'integer',
            columnName: 'game_id'
        },
        player_id: 'integer',
        min: 'string',
        tpm: 'integer',
        fta: 'integer',
        ftm: 'integer',
        pf: 'integer',
        pts: 'integer',
        tf: 'integer',
        uf: 'integer',
        df: 'integer',
        isStarter: 'integer',
        season_id: 'integer',
        league_id: 'integer',
    }
};

