module.exports = {
    attributes: {
        firstname: 'string',
        lastname: 'string',
        height: 'int',
        birthdate: 'date'
    }
};