'use strict';

var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
    dbm = options.dbmigrate;
    type = dbm.dataType;
    seed = seedLink;
};

exports.up = function (db) {
    db.addColumn('games', 'stage', {type: 'int', unsigned: true, notNull: true, length: 1, defaultValue : 1});
    db.addColumn('games', 'round', {type: 'int', unsigned: true, notNull: true, length: 1, defaultValue : 1});
    return null;
};

exports.down = function (db) {
    return null;
};
