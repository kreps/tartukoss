'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
    db.removeColumn('players', 'nationality');
    db.removeColumn('players', 'startyear');
    db.removeColumn('players', 'firstclub');
    db.removeColumn('players', 'firstcoach');
    db.removeColumn('players', 'weight');
    db.removeColumn('players', 'position');
    db.removeColumn('players', 'birthplace');
    db.removeColumn('players', 'nicknames');
    db.removeColumn('players', 'achievements');
    db.removeColumn('players', 'education');
    db.removeColumn('players', 'maritalstatus');
    db.removeColumn('players', 'kids');
    db.removeColumn('players', 'hobbies');
    db.removeColumn('players', 'example');
    db.renameColumn('players', 'pid', 'id');

    db.renameTable('team_names', 'teamnames');
    db.renameColumn('teamnames', 'nid', 'id');
    db.removeColumn('teamnames', 'abbreviation2');

    db.renameTable('championships', 'leagues');
    db.renameColumn('leagues', 'chid', 'id');

    db.renameColumn('players_teams', 'sid', 'seasonId');
    db.renameColumn('players_teams', 'pid', 'playerId');
    db.renameColumn('players_teams', 'chid', 'leagueId');
    db.renameColumn('players_teams', 'tid', 'teamId');

    db.renameColumn('teams', 'sid', 'seasonId');
    db.renameColumn('teams', 'nid', 'teamnameId');
    db.renameColumn('teams', 'chid', 'leagueId');
    db.renameColumn('teams', 'tid', 'teamId');
    db.removeColumn('teams', 'plid');
    db.removeColumn('teams', 'plid1');


    db.renameTable('games', 'boxscores');
    db.renameColumn('boxscores', 'gid', 'gameId');
    db.renameColumn('boxscores', 'pid', 'playerId');
    db.renameColumn('boxscores', 'tid', 'teamId');
    db.renameColumn('boxscores', 'pm3', 'threePointShotCount');
    db.renameColumn('boxscores', 'ftma', 'freeThrowsAttempts');
    db.renameColumn('boxscores', 'ftm', 'freeThrowsMade');
    db.renameColumn('boxscores', 'pf', 'personalFouls');
    db.renameColumn('boxscores', 'pts', 'pointsMade');
    db.renameColumn('boxscores', 'tn_f', 'technicalFouls');
    db.renameColumn('boxscores', 'un_f', 'unsportsmFouls');
    db.renameColumn('boxscores', 'dq_f', 'disqualifyingFouls');
    db.renameColumn('boxscores', 'st_l', 'isStarter');
    db.removeColumn('boxscores', 'pma2');
    db.removeColumn('boxscores', 'pm2');
    db.removeColumn('boxscores', 'pma2o');
    db.removeColumn('boxscores', 'pm2o');
    db.removeColumn('boxscores', 'pma3');
    db.removeColumn('boxscores', 'dreb');
    db.removeColumn('boxscores', 'oreb');
    db.removeColumn('boxscores', 'reb');
    db.removeColumn('boxscores', 'ast');
    db.removeColumn('boxscores', 'asta');
    db.removeColumn('boxscores', 'pffp');
    db.removeColumn('boxscores', 'st');
    db.removeColumn('boxscores', 'bs');
    db.removeColumn('boxscores', 'tov');
    db.removeColumn('boxscores', 'fi');
    db.removeColumn('boxscores', 'nr');
    db.removeColumn('boxscores', 'kkey');

    db.renameTable('results', 'gamescores');
    db.renameColumn('gamescores', 'gid', 'gameId');
    db.renameColumn('gamescores', 'tid', 'teamId');
    db.renameColumn('gamescores', 'chid', 'leagueId');
    db.renameColumn('gamescores', 'hv', 'isVisitor');
    db.renameColumn('gamescores', 'pts', 'pointsTotal');
    db.renameColumn('gamescores', 'pts1', 'points1q');
    db.renameColumn('gamescores', 'pts2', 'points2q');
    db.renameColumn('gamescores', 'pts3', 'points3q');
    db.renameColumn('gamescores', 'pts4', 'points4q');
    db.renameColumn('gamescores', 'pts5', 'points5q');
    db.renameColumn('gamescores', 'pts6', 'points6q');
    db.renameColumn('gamescores', 'pts7', 'points7q');
    db.renameColumn('gamescores', 'pts8', 'points8q');
    db.renameColumn('gamescores', 'p', 'pointsTournament');
    db.renameColumn('gamescores', 'tn_f', 'technicalFouls');
    db.renameColumn('gamescores', 'dq_f', 'disqualifyingFouls');
    db.removeColumn('gamescores', 'dreb');
    db.removeColumn('gamescores', 'oreb');
    db.removeColumn('gamescores', 'reb');
    db.removeColumn('gamescores', 'ast');
    db.removeColumn('gamescores', 'tov');
    db.removeColumn('gamescores', 'qa');
    db.removeColumn('gamescores', 'qm');

    db.renameTable('timetable', 'schedule');
    db.renameColumn('schedule', 'gid', 'gameId');
    db.renameColumn('schedule', 'chid', 'leagueId');
    db.renameColumn('schedule', 'h_tid', 'homeTeamId');
    db.renameColumn('schedule', 'v_tid', 'visitorTeamId');
    db.renameColumn('schedule', 'plid', 'placeId');
    db.renameColumn('schedule', 'href', 'referee1');
    db.renameColumn('schedule', 'ref', 'referee2');
    db.renameColumn('schedule', 'ref1', 'referee3');
    db.renameColumn('schedule', 'attend', 'attendants');
    db.removeColumn('schedule', 'com');
    db.removeColumn('schedule', 'hsecr');
    db.removeColumn('schedule', 'secr');
    db.removeColumn('schedule', 'tv');
    db.removeColumn('schedule', 'link');
    db.removeColumn('schedule', 'curr_per');
    db.removeColumn('schedule', 'curr_sec');
    db.removeColumn('schedule', 'status');

    db.renameColumn('coaches', 'cid', 'id');
    db.removeColumn('coaches', 'level');
    db.removeColumn('coaches', 'birthdate');
    db.removeColumn('coaches', 'phone');
    db.removeColumn('coaches', 'email');
    db.removeColumn('coaches', 'birthplace');
    db.removeColumn('coaches', 'nicknames');
    db.removeColumn('coaches', 'c_education');
    db.removeColumn('coaches', 'achievements');
    db.removeColumn('coaches', 'startyear');
    db.removeColumn('coaches', 'education');
    db.removeColumn('coaches', 'maritalstatus');
    db.removeColumn('coaches', 'kids');
    db.removeColumn('coaches', 'hobbies');

    db.renameColumn('coaches_teams', 'sid', 'seasonId');
    db.renameColumn('coaches_teams', 'cid', 'coachId');
    db.renameColumn('coaches_teams', 'chid', 'leagueId');
    db.renameColumn('coaches_teams', 'tid', 'teamId');

    db.renameColumn('places', 'plid', 'id');
    db.removeColumn('places', 'sport_events');
    db.removeColumn('places', 'phone');
    db.removeColumn('places', 'attendance');
    db.removeColumn('places', 'category');

    db.renameTable('periods', 'seasons');
    db.removeColumn('seasons', 'uid');
  return null;
};

exports.down = function(db) {
  return null;
};
