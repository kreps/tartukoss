'use strict';

var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
    dbm = options.dbmigrate;
    type = dbm.dataType;
    seed = seedLink;
};

exports.up = function (db) {
    db.renameColumn('teams', 'seasonId', 'season');
    db.renameColumn('teams', 'teamId', 'team');
    db.renameColumn('teams', 'leagueId', 'leagueName');
    db.renameColumn('teams', 'teamnameId', 'name');
    db.renameColumn('teams', 'leaguetypeid', 'league');
    return null;
};

exports.down = function (db) {
    return null;
};
