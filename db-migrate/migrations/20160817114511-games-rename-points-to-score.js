'use strict';

var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
    dbm = options.dbmigrate;
    type = dbm.dataType;
    seed = seedLink;
};

exports.up = function (db) {
    db.renameColumn('games', 'pointsTotal', 'score');
    db.renameColumn('games', 'points1q', 'score1q');
    db.renameColumn('games', 'points2q', 'score2q');
    db.renameColumn('games', 'points3q', 'score3q');
    db.renameColumn('games', 'points4q', 'score4q');
    db.renameColumn('games', 'points5q', 'score5q');
    db.renameColumn('games', 'points6q', 'score6q');
    db.renameColumn('games', 'points7q', 'score7q');
    db.renameColumn('games', 'points8q', 'score8q');
    return null;
};

exports.down = function (db) {
    return null;
};