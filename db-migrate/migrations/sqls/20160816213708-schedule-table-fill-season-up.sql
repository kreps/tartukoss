UPDATE schedule SET season = 2009 where gameId > 2009000000 and gameId < 2009999999;
UPDATE schedule SET season = 2010 where gameId > 2010000000 and gameId < 2010999999;
UPDATE schedule SET season = 2011 where gameId > 2011000000 and gameId < 2011999999;
UPDATE schedule SET season = 2012 where gameId > 2012000000 and gameId < 2012999999;
UPDATE schedule SET season = 2013 where gameId > 2013000000 and gameId < 2013999999;
UPDATE schedule SET season = 2014 where gameId > 2014000000 and gameId < 2014999999;
UPDATE schedule SET season = 2015 where gameId > 2015000000 and gameId < 2015999999;
UPDATE schedule SET season = 2016 where gameId > 2016000000 and gameId < 2016999999;
UPDATE schedule SET season = 2017 where gameId > 2017000000;

DELETE FROM  place_season_league;

INSERT INTO place_season_league (season, league, place)
  (SELECT season, leaguetype, placeId FROM schedule GROUP BY season, leaguetype, placeId);