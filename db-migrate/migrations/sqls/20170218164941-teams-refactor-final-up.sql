ALTER TABLE teams
  CHANGE abbreviation abbr VARCHAR(15) NOT NULL DEFAULT '',
  ENGINE = InnoDB;

ALTER TABLE teams_season_league
  CHANGE team_id team_id_drop SMALLINT(4) UNSIGNED NOT NULL,
  CHANGE name team SMALLINT(4) UNSIGNED,
  CHANGE leagueName _leaguename_drop SMALLINT(3) UNSIGNED,
  ADD COLUMN name VARCHAR(60) DEFAULT NULL,
  ADD COLUMN abbr VARCHAR(15) DEFAULT NULL,
  ENGINE = InnoDB;

UPDATE teams_season_league
  INNER JOIN teams ON teams_season_league.team = teams.id
SET
  teams_season_league.name = teams.name,
  teams_season_league.abbr = teams.abbr;

DELETE FROM teams_season_league
WHERE id = 142;

ALTER TABLE teams
  CONVERT TO CHARACTER SET utf8
  COLLATE utf8_unicode_ci;

ALTER TABLE teams_season_league
  CONVERT TO CHARACTER SET utf8
  COLLATE utf8_unicode_ci;

ALTER TABLE teams_season_league
  ADD CONSTRAINT teams_season_league_teams_id_fk
FOREIGN KEY (team) REFERENCES teams (id);

ALTER TABLE games
  ADD COLUMN home SMALLINT(4) UNSIGNED,
  ADD COLUMN visitor SMALLINT(4) UNSIGNED,
  CONVERT TO CHARACTER SET utf8
  COLLATE utf8_unicode_ci,
  ENGINE = InnoDB;

UPDATE games g
  INNER JOIN teams_season_league tsl ON g.homeTeamId = tsl.team_id_drop
SET g.home = tsl.team;

UPDATE games g
  INNER JOIN teams_season_league tsl ON g.visitorTeamId = tsl.team_id_drop
SET g.visitor = tsl.team;


ALTER TABLE games
  CHANGE homeTeamId _home_drop SMALLINT(4) UNSIGNED,
  CHANGE visitorTeamId _visitor_drop SMALLINT(4) UNSIGNED;

ALTER TABLE dwapi.games
  ADD CONSTRAINT games_teams_home_id_fk
FOREIGN KEY (home) REFERENCES teams (id);

ALTER TABLE dwapi.games
  ADD CONSTRAINT games_teams_visitor_id_fk
FOREIGN KEY (visitor) REFERENCES teams (id);

ALTER TABLE gameresults
  ADD COLUMN team SMALLINT(4) UNSIGNED,
  CONVERT TO CHARACTER SET utf8
  COLLATE utf8_unicode_ci,
  ENGINE = InnoDB;

UPDATE gameresults g
  INNER JOIN teams_season_league tsl ON g.teamId = tsl.team_id_drop
SET g.team = tsl.team;