ALTER TABLE leagues
  CHANGE abbreviation abbr VARCHAR(10);

ALTER TABLE places
  CHANGE abbreviation abbr VARCHAR(10);

UPDATE places SET name = 'Kõrveküla sh', abbr = 'Kõrveküla', link = 'https://goo.gl/maps/NMNutC3yKfp' WHERE id = 1;
UPDATE places SET name = 'Eesti Maaülikooli sh', abbr = 'EMÜ', address = 'Kreutzwaldi 3, Tartu', link = 'https://goo.gl/maps/dDs49kWUXFM2' WHERE id = 2;
UPDATE places SET name = 'Ülenurme Gümnaasiumi sh', abbr = 'Ülenurme', address = 'Tartu mnt 5, Ülenurme alevik, Tartumaa',
  link                 = 'https://goo.gl/maps/njEMQfkKzpz' WHERE id = 3;
UPDATE places SET name = 'Tartu Ülikooli sh', abbr = 'TÜ', address = 'Ujula 4, Tartu', link = 'https://goo.gl/maps/4oxpdE6Rqss' WHERE id = 4;
UPDATE places SET name = 'Turu sh', abbr = 'Turu', address = 'Turu 8, Tartu', link = 'https://goo.gl/maps/vKWRg6dq6CH2' WHERE id = 5;
UPDATE places SET name = 'Puhja Gümnaasiumi sh', abbr = 'Puhja', address = 'Viljandi tee 28/1, Puhja alevik, Tartumaa',
  link                 = 'https://goo.gl/maps/dpX9zhGRPUR2' WHERE id = 6;
UPDATE places SET name = 'Kambja sh', abbr = 'Kambja', link = 'https://goo.gl/maps/2QeNwyWHJ832' WHERE id = 7;
UPDATE places SET name = 'Vanemuise sh', link = 'https://goo.gl/maps/XX9jCeTRTpH2', abbr = 'Vanemuise' WHERE id = 8;
UPDATE places SET name = 'Nõo sh', abbr='Nõo', link = 'https://goo.gl/maps/MLabhXKEpwy' WHERE id = 10;
UPDATE places SET name = 'Võnnu kool', abbr='Võnnu', link = 'https://goo.gl/maps/YEvtR67v3322' WHERE id = 11;
UPDATE places SET name = 'Lähte sh', abbr='Lähte', link = 'https://goo.gl/maps/VBeZrsZFFbL2' WHERE id = 12;
UPDATE places SET name = 'Luunja sh', abbr='Luunja', link = 'https://goo.gl/maps/PWj7UjXSxxt' WHERE id = 13;
UPDATE places SET name = 'Torma sh', abbr = 'Torma', link = 'https://goo.gl/maps/Nr68MF32pfH2' WHERE id = 14;
UPDATE places SET name = 'Tabivere sh', abbr = 'Tabivere', link = 'https://goo.gl/maps/Xzi32mNdsbL2' WHERE id = 15;
UPDATE places SET name = 'Visa hall', abbr = 'Visa', link = 'https://goo.gl/maps/K7Cm7iPP9K32' WHERE id = 16;
UPDATE places SET name = 'A. Le Coq sh', abbr = 'Alecoq', address = 'Ihaste tee 7, Tartu', link = 'https://goo.gl/maps/aAEAK2C46nt' WHERE id = 17;
UPDATE places SET name = 'KHK võimla', abbr = 'KHK', link = 'https://goo.gl/maps/kJxDaNkpphw' WHERE id = 18;
UPDATE places SET name = 'Kammeri kool', abbr = 'Kammeri', link = 'https://goo.gl/maps/xB2Ez5bEZL42' WHERE id = 19;
UPDATE places SET name = 'Tartu Kesklinna Kool', abbr = 'Kesklinna', address = 'Kroonuaia 7, Tartu', link = 'https://goo.gl/maps/CkGv7K5qqwC2' WHERE id = 20;
UPDATE places SET name = 'Vara sh', abbr = 'Vara', address = 'Vara küla, Vara vald, Tartumaa', link = 'https://goo.gl/maps/DfaCN4hgpmN2' WHERE id = 21;