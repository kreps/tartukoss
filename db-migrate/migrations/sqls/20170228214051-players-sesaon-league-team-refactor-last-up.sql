RENAME TABLE
    players_teams_season_league TO players_season_league_team;

ALTER TABLE players_season_league_team
  CHANGE seasonId season SMALLINT(4) UNSIGNED,
  CHANGE playerId player SMALLINT(5) UNSIGNED,
  DROP leagueId,
  CHANGE team_id team INT(11),
  DROP nr;

UPDATE
    players_season_league_team p
    INNER JOIN
    teams_season_league t ON (p.league = t.league
                              AND p.season = t.season
                              AND p.teamId_old = t.team_id_drop)
SET
  p.team = t.team;

UPDATE gameresults
SET team = 80
WHERE season = 2016 AND league = 3 AND teamId = 80;

UPDATE gameresults
SET team = 121
WHERE season=2016 and league=3 and teamId=121;