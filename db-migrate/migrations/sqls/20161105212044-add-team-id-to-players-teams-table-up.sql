UPDATE players_teams
  INNER JOIN
  teams ON (players_teams.league = teams.league
            AND players_teams.seasonId = teams.season
            and players_teams.teamId_old = teams.team)
SET
  players_teams.team_id = teams.id;
