ALTER TABLE leagues
  CHANGE abbr abbreviation VARCHAR(255);

ALTER TABLE places
  CHANGE abbr abbreviation VARCHAR(255);