# Simple tables
# players
# coaches
RENAME TABLE leagues TO leaguenames_old;
RENAME TABLE leaguetypes TO leagues;
# places
# referees
# seasons
RENAME TABLE stage TO stages;
RENAME TABLE teams TO teams_season_league;
RENAME TABLE teamnames TO teams;

# Binding tables
RENAME TABLE coaches_teams TO coaches_teams_season_league;
RENAME TABLE place_season_league TO places_season_league;
RENAME TABLE players_teams TO players_teams_season_league;

