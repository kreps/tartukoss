ALTER TABLE teams_season_league
  DROP FOREIGN KEY teams_season_league_teams_id_fk,
  CHANGE team_id_drop team_id SMALLINT(4) UNSIGNED NOT NULL,
  CHANGE team name SMALLINT(4) UNSIGNED,
  CHANGE _leaguename_drop leagueName SMALLINT(3) UNSIGNED,
  DROP name,
  DROP abbr;

ALTER TABLE teams
  CHANGE abbr abbreviation VARCHAR(10) NOT NULL DEFAULT '';

ALTER TABLE games
  DROP home,
  DROP visitor;

ALTER TABLE games
  CHANGE _home_drop homeTeamId SMALLINT(4) UNSIGNED,
  CHANGE _visitor_drop visitorTeamId SMALLINT(4) UNSIGNED;