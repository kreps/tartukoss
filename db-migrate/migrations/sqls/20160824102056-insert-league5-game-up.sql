-- insert into schedule (id, round, stage, homeTeamId, visitorTeamId, league, season) values(2011011042, 2, 1, 121, 117, 5, 2011);

insert into games (gameId, round, stage, teamId, points, league, season, score, isVisitor)
values (2011011042, 2, 1, 121, 2, 5, 2011, 20, 0);

insert into games (gameId, round, stage, teamId, points, league, season, score, isVisitor)
values (2011011042, 2, 1, 117, 0, 5, 2011, 0, 1);