/*Add new column team_id Foreignkey teams.id*/
ALTER TABLE dwapi.players_teams CHANGE teamId teamId_old SMALLINT(4) unsigned NOT NULL DEFAULT '0';
ALTER TABLE dwapi.players_teams ADD team_id INT DEFAULT -1 NOT NULL;