ALTER TABLE teams_season_league
  MODIFY COLUMN id INT(5) unsigned NOT NULL AUTO_INCREMENT FIRST,
  CHANGE team team_id SMALLINT(4) unsigned NOT NULL,
  ENGINE=InnoDb;