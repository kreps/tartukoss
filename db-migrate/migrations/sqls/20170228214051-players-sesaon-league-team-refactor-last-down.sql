ALTER TABLE players_season_league_team
  CHANGE  season seasonId SMALLINT(4) UNSIGNED,
  CHANGE  player playerId SMALLINT(5) UNSIGNED,
  ADD leagueId SMALLINT(3) UNSIGNED,
  CHANGE team team_id  SMALLINT(4) UNSIGNED,
  ADD nr TINYINT(2);

RENAME TABLE
    players_season_league_team TO players_teams_season_league;

