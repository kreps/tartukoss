ALTER TABLE boxscores
  CHANGE gameId game_id INT(10) UNSIGNED ZEROFILL NOT NULL,
  CHANGE playerId player_id SMALLINT(5) UNSIGNED NOT NULL,
  CHANGE teamId teamId_old SMALLINT(4) UNSIGNED,
  CHANGE threePointShotCount tpm TINYINT(2) unsigned COMMENT '3pm',
  CHANGE freeThrowsAttempts fta TINYINT(2) UNSIGNED,
  CHANGE freeThrowsMade ftm TINYINT(2) UNSIGNED,
  CHANGE personalFouls pf TINYINT(2) UNSIGNED COMMENT 'Personal fouls',
  CHANGE pointsMade pts TINYINT(2) UNSIGNED COMMENT 'Points made',
  CHANGE technicalFouls tf TINYINT(1) UNSIGNED COMMENT 'Technical fouls',
  CHANGE unsportsmFouls uf TINYINT(1) UNSIGNED COMMENT 'Unsportsmanlike fouls',
  CHANGE disqualifyingFouls df TINYINT(1) UNSIGNED COMMENT 'Disqualifing fouls',
  ADD COLUMN season_id SMALLINT(4) DEFAULT NULL,
  ADD COLUMN league_id SMALLINT(4) DEFAULT NULL;

UPDATE boxscores SET season_id = 2009 where game_id > 2009000000 and game_id < 2009999999;
UPDATE boxscores SET season_id = 2010 where game_id > 2010000000 and game_id < 2010999999;
UPDATE boxscores SET season_id = 2011 where game_id > 2011000000 and game_id < 2011999999;
UPDATE boxscores SET season_id = 2012 where game_id > 2012000000 and game_id < 2012999999;
UPDATE boxscores SET season_id = 2013 where game_id > 2013000000 and game_id < 2013999999;
UPDATE boxscores SET season_id = 2014 where game_id > 2014000000 and game_id < 2014999999;
UPDATE boxscores SET season_id = 2015 where game_id > 2015000000 and game_id < 2015999999;
UPDATE boxscores SET season_id = 2016 where game_id > 2016000000 and game_id < 2016999999;
UPDATE boxscores SET season_id = 2017 where game_id > 2017000000;

UPDATE boxscores SET league_id = 1 where substr( game_id, 5, 3) in (002, 005, 008, 014);
UPDATE boxscores SET league_id = 2 where substr( game_id, 5, 3) in (003, 006, 009, 015);
UPDATE boxscores SET league_id = 3 where substr( game_id, 5, 3) in (004, 007, 010, 016);
UPDATE boxscores SET league_id = 4 where substr( game_id, 5, 3) in (001, 012, 013, 017, 018);
UPDATE boxscores SET league_id = 5 where substr( game_id, 5, 3) in (011);
UPDATE boxscores SET league_id = 6 where substr( game_id, 5, 3) in (019);
