RENAME TABLE
    schedule TO games;

ALTER TABLE games
  ADD COLUMN datetime DATETIME DEFAULT NULL;

UPDATE games
SET datetime = concat(date, ' ', time);

# ALTER TABLE games
#   DROP date,
#   DROP time;
