ALTER TABLE leagues CHANGE `order` sort SMALLINT(6);

INSERT INTO leagues (id, name, abbr, sort) VALUES (6, 'Super Karikas', 'SK', 0);

UPDATE leagues SET sort = 1 WHERE id = 4;
UPDATE leagues SET sort = 2 WHERE id = 1;
UPDATE leagues SET sort = 3 WHERE id = 2;
UPDATE leagues SET sort = 4 WHERE id = 3;
UPDATE leagues SET sort = 5 WHERE id = 5;

UPDATE games SET league = 6, stage= 3 WHERE leagueId = 19;
UPDATE gameresults SET league = 6 WHERE leagueId = 19;

DELETE FROM places_season_league;
INSERT INTO places_season_league (season, league, place)
  SELECT season, league, placeId FROM games GROUP BY season, league, placeId;