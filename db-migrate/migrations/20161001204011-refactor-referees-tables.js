'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
    db.renameColumn('referees', 'rid', 'id');
    db.removeColumn('referees', 'birthplace');
    db.removeColumn('referees', 'job');
    db.removeColumn('referees', 'education');
    db.removeColumn('referees', 'maritalstatus');
    db.removeColumn('referees', 'kids');
    db.removeColumn('referees', 'hobbies');

    db.renameTable('referees_champ', 'referees_season');
    db.renameColumn('referees_season', 'rid', 'referee');
    db.renameColumn('referees_season', 'sid', 'season');
    return null;
};

exports.down = function(db) {
  return null;
};
