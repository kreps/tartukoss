module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        concat: {
            controllers: {
                src: ['app/controllers/**/*.js'],
                dest: 'app/dist/controllers.js',
            },
        }
    });

    // Load the plugin
    grunt.loadNpmTasks('grunt-contrib-concat');

    // Default task(s).
    grunt.registerTask('default', ['concat']);

};