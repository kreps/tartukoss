'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', ['ngRoute', 'ngCookies',
    'ngMaterial',
    'myApp.view1',
    'myApp.view2',
    'myApp.places',
    'myApp.referees',
    'myApp.standings',
    'myApp.version'

])
// .config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
//     $locationProvider.hashPrefix('!');

//     $routeProvider.otherwise({
//         redirectTo: '/view1'
//     });
// }])
    .controller('MainCtrl', function ($scope, $http, $filter, $location, $q, $cookies, $routeParams, $rootScope) {
        $scope.ismobile = window.innerWidth < 900;
        // var apiuri = "http://46.101.192.210";
        var apiuri = "http://localhost";
        $scope.apiuri = apiuri;
        var seasons = $http.get(apiuri + ":1337/api/seasons?sort=id desc", {cache: true}),
            leagues = $http.get(apiuri + ":1337/api/leagues?sort=order asc", {cache: true}),
            teamnames = $http.get(apiuri + ":1337/api/teams", {cache: true});

        $q.all([seasons, leagues]).then(function (res) {
            $rootScope.seasons = res[0].data;
            $rootScope.leaguetypes = res[1].data;
            $rootScope.seasonId = $rootScope.seasons[0].id;
            $rootScope.seasonName = $rootScope.seasons[0].name;
            $rootScope.leagueId = $rootScope.leaguetypes[0].id;
            $rootScope.leagueName = $rootScope.leaguetypes[0].name;

            if ($routeParams.season) {
                $rootScope.seasonId = $routeParams.season;
                $rootScope.seasonName = $routeParams.season;
            }

            if ($routeParams.league) {
                $rootScope.leagueId = $routeParams.league;
                $rootScope.leagueName = $routeParams.league;
            }
        });

        $scope.change = function () {
            $location.path('/');
        };

        $scope.getTeam = function (id) {
            $scope.team = [];
            var team = $http.get(apiuri + ":1337/api/teams/" + id);
            var players = $http.get(apiuri + ":1337/api/players_teams?team_id=" + id);

            $q.all([team, players]).then(function (response) {
                $scope.team = response[0].data;
                $scope.team.players = response[1].data;
            });
        };

        $scope.getPlaces = function () {
            $scope.places = [];
            $http.get(apiuri + ":1337/api/place_season_league?league=" + $scope.selectedLeague.id + "&season=" + $scope.selectedSeason.id)
                .then(function (response) {
                    $scope.places = response.data;
                });
        };

        $scope.getReferees = function () {
            $scope.referees = [];
            $http.get(apiuri + ":1337/api/referees")
                .then(function (response) {
                    $scope.referees = response.data;
                });
        };

        $scope.getGames = function () {
            $scope.games = [];
            $http.get(apiuri + ":1337/api/gameresults?league=" + $scope.selectedLeague.id + "&season=" + $scope.selectedSeason.id)
                .then(function (response) {
                    $scope.games = response.data;
                });
        };

        function countRivalty(s) {
            for (var i = 0; i < s.length; i++) {
                for (var j = 1; j < s.length; j++) {
                    if (s[i] && s[j] && s[i].points == s[j].points && s[i].teams[s[j].id]) {
                        s[i].rivalty += s[i].teams[s[j].id].wins;
                        if (!s[i].rivaltydiff)
                            s[i].rivaltydiff = 0;
                        s[i].rivaltydiff += s[i].teams[s[j].id].diff;
                    }
                }
            }
        }

        function setTeamPoints(s, g0, g1) {
            if (!s[g0.teamId]) {
                s[g0.teamId] = {
                    id: g0.teamId,
                    points: g0.points,
                    rivalty: 0,
                    count: 1,
                    teams: [],
                    score: g0.score,
                    scoreop: g1.score,
                    wins: g0.points == 2 ? 1 : 0
                };
            } else {
                s[g0.teamId].points += g0.points;
                s[g0.teamId].count++;
                s[g0.teamId].score += g0.score;
                s[g0.teamId].scoreop += g1.score;
                s[g0.teamId].wins += g0.points == 2 ? 1 : 0;
            }
            //set wins(true,false) team by team
            if (!s[g0.teamId].teams[g1.teamId]) {
                s[g0.teamId].teams[g1.teamId] = {};
                s[g0.teamId].teams[g1.teamId].wins = 0;
                s[g0.teamId].teams[g1.teamId].diff = 0;
            }
            if (g0.points == 2) {
                s[g0.teamId].teams[g1.teamId].wins++;
            }
            s[g0.teamId].teams[g1.teamId].diff += (g0.score - g1.score);
        }

        function setStandingsArray() {
            var s = [];
            for (var k = 0; k < $scope.schedule.length; k++) {
                var event = $scope.schedule[k];
                // var team0 =
                var g0 = event.games[0];
                var g1 = event.games[1];
                // var tn = team.name.name;
                setTeamPoints(s, g0, g1);
                setTeamPoints(s, g1, g0);
            }

            countRivalty(s);

            for (var i = 0; i < s.length; i++) {
                if (s[i]) {
                    s[i].avg = Math.round(s[i].score * 10 / s[i].count) / 10;
                    s[i].avgop = Math.round(s[i].scoreop * 10 / s[i].count) / 10;
                    s[i].winperc = Math.round(s[i].wins * 1000 / s[i].count) / 10;
                    s[i].name = $scope.getTeamByTeamId(i).name.name;
                    s[i].scorediff = s[i].score - s[i].scoreop;
                    $scope.standings.push(s[i]);
                }
            }

            console.log($scope.standings);

        }

        $scope.calculateStandings = function () {
            $scope.teams = [];
            $scope.standings = [];
            var teamsGet = $http.get(apiuri + ":1337/api/teams?league=" + $scope.selectedLeague.id + "&season=" + $scope.selectedSeason.id),
                scheduleGet = $http.get(apiuri + ":1337/api/schedule?league=" + $scope.selectedLeague.id + "&season=" + $scope.selectedSeason.id + "&stage=1");

            $q.all([teamsGet, scheduleGet]).then(function (response) {
                $scope.teams = response[0].data;
                $scope.schedule = response[1].data;
                setStandingsArray();
            });
        };



        $scope.getScheduleById = function (gameid) {
            return $filter('filter')($scope.schedule, {
                id: gameid
            })[0];
        };

        $scope.buildSearchData = buildSearchData;

        function buildSearchData(event) {
            return event.home + ' ' + event.visitor + ' ' + event.stage.name;
        }

        $scope.go = function (url) {
            $location.path(url);
        }

    })
    .filter('getByProperty', function() {
        return function(propertyName, propertyValue, collection) {
            var i=0, len=collection.length;
            for (; i<len; i++) {
                if (collection[i][propertyName] == +propertyValue) {
                    return collection[i];
                }
            }
            return null;
        }
    });
function setSeasonName(text) {
    document.getElementById('seasonName').innerHTML = text;
}

function setLeagueName(text) {
    document.getElementById('leagueName').innerHTML = text;
}