'use strict';

angular.module('myApp.standings', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/standings', {
            templateUrl: 'standings/standings.html',
            controller: 'StandingsCtrl'
        });
    }])

    .controller('StandingsCtrl', ['$scope', function ($scope) {
        $scope.calculateStandings();
    }]);