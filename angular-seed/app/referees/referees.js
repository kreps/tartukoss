'use strict';

angular.module('myApp.referees', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/referees', {
            templateUrl: 'referees/list.html',
            controller: 'RefereesCtrl'
        });
    }])

    .controller('RefereesCtrl', ['$scope', function ($scope) {
        $scope.getReferees();
    }]);