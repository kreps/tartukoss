angular.module('myApp')
.config(
    function($routeProvider) {
        $routeProvider
            .when('/test', {templateUrl: 'test.html'})

            // .when('/:season/:league', {redirectTo: '/:season/:league/teams'})
            .when('/players', { templateUrl: 'templates/pages/players/list.html', controller: 'PlayersCtrl'})
            .when('/:season/:league/teams', { templateUrl: 'templates/pages/teams/list.html', controller: 'TeamsListCtrl as TeamsCrtl'})
            .when('/:season/:league/games', { templateUrl: 'templates/pages/games/list.html', controller: 'GamesListCtrl as GamesCtrl'})
            .when('/teams/:id', { templateUrl: 'templates/pages/teams/show.html', controller: 'TeamShowCtrl', controllerAs: 'TeamShowCtrl'})
            .when('/:season/:league', {templateUrl: 'test.html', controller: 'SeasonLeagueRouteCtrl'})
            .otherwise( {redirectTo: '/'} );
    });
