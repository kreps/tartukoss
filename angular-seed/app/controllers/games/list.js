angular.module('myApp').controller('GamesListCtrl', function ($scope, $http, $rootScope, $q, $filter) {
    var apiuri = $scope.apiuri;
    var schedule = $http.get(apiuri + ":1337/api/schedule?season=" + $rootScope.seasonId+"&league=" + $rootScope.leagueId),
        teams = $http.get(apiuri + ":1337/api/teams_season_league?season=" + $rootScope.seasonId),
        teamnames = $http.get(apiuri + ":1337/api/teams");

    function setHomeVisitor(i) {
        var si = $scope.schedule[i];
        si.home = getTeamByTeamId(si.homeTeamId).name.name;
        si.visitor = getTeamByTeamId(si.visitorTeamId).name.name;
        var homegame = $scope.schedule[i].games[0].teamId == si.homeTeamId ? $scope.schedule[i].games[0] : $scope.schedule[i].games[1];
        var visigame = $scope.schedule[i].games[0].teamId == si.visitorTeamId ? $scope.schedule[i].games[0] : $scope.schedule[i].games[1];
        si.homeScore = homegame.score;
        si.visitorScore = visigame.score;
    }

    var getTeamByTeamId = function (teamId) {
        return $filter('filter')($scope.teams, function (t) {
            return t.team === teamId;
        })[0];
    };

    $q.all([schedule, teams, teamnames]).then(function (response) {
        $scope.schedule = response[0].data;
        $scope.teams = response[1].data;
        $scope.teamnames = response[2].data;
        for (var i = 0; i < $scope.schedule.length; ++i) {
            setHomeVisitor(i);
        }
    });
});