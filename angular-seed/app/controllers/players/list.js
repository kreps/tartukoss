angular.module('myApp').controller('PlayersCtrl', function ($scope, $http) {
    var controller = this;
    $http.get($scope.apiuri + ":1337/api/players_teams?league=" + $scope.selectedLeague.id + "&seasonId=" + $scope.selectedSeason.id)
    .then(function (response) {
        controller.players = response.data;
    });
});