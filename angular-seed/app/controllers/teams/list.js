angular.module('myApp').controller('TeamsListCtrl', function ($scope, $http, $routeParams) {
    var ctrl = this;
    var season = $routeParams.season;
    var league = $routeParams.league;
    var url = $scope.apiuri + ":1337/api/teams?";

    if (league) {
        url += "league=" + league + "&";
    }
    $http.get(url + "&season=" + season).then(function (response) {
        ctrl.teams = response.data;
    });
});
