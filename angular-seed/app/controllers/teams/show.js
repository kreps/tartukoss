angular.module('myApp').controller('TeamShowCtrl', function($scope, $http, $routeParams, $q){
    var ctrl = this;
    var id = $routeParams.id;
    var team = $http.get($scope.apiuri + ":1337/api/teams/" + id);
    var players = $http.get($scope.apiuri + ":1337/api/players_teams?team_id=" + id);

    $q.all([team, players]).then(function (response) {
        ctrl.team = response[0].data;
        ctrl.team.players = response[1].data;
    });
});
