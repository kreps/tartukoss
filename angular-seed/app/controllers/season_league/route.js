angular.module('myApp').controller('SeasonLeagueRouteCtrl', function ($scope, $routeParams, $rootScope, $filter) {
    $rootScope.leagueId = $routeParams.league;
    $rootScope.seasonId = $routeParams.season;
    $rootScope.seasonName = $routeParams.season - 1 + "/" + $routeParams.season;
    $rootScope.leagueName = $filter('getByProperty')('id', $routeParams.league, $rootScope.leaguetypes).name;
});
