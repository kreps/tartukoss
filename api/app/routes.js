var bears = require('./models/bear');
var players = require('./models/players');

module.exports = {
    configure: function(app) {
        app.get('/bears', function(req, res) {
            bears.get(res);
        });

        app.get('/bears/:id', function(req, res) {
            bears.getOne(req.params.id, res);
        });

        app.post('/bears', function(req, res) {
            bears.create(req.body, res);
        });

        app.put('/bears', function(req, res) {
            bears.update(req.body, res);
        });

        app.delete('/bears/:id/', function(req, res) {
            players.delete(req.params.id, res);
        });

        app.get('/players', function(req, res) {
            players.get(res);
        });

        app.get('/players/:id', function(req, res) {
            players.getOne(req.params.id, res);
        });

        app.post('/players', function(req, res) {
            players.create(req.body, res);
        });

        app.put('/players', function(req, res) {
            players.update(req.body, res);
        });

        app.delete('/bears/:id/', function(req, res) {
            bears.delete(req.params.id, res);
        });
    }
};