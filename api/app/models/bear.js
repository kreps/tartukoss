var connection = require('../connection');

function Bear() {
    this.get = function(res) {
        connection.acquire(function(err, con) {
            con.query('select * from bears', function(err, result) {
                con.release();
                res.send(result);
            });
        });
    };

    this.getOne = function(id, res) {
        connection.acquire(function(err, con) {
            con.query('select * from bears where id = ?', [id], function(err, result) {
                con.release();
                res.send(result);
            });
        });
    };

    this.delete = function(id, res) {
        connection.acquire(function(err, con) {
            con.query('delete from todo_list where id = ?', [id], function(err, result) {
                con.release();
                if (err) {
                    res.send({status: 1, message: 'Failed to delete'});
                } else {
                    res.send({status: 0, message: 'Deleted successfully'});
                }
            });
        });
    };
}

module.exports = new Bear();