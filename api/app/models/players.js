var connection = require('../connection');

function Player() {
    this.get = function(res) {
        connection.acquire(function(err, con) {
            con.query('select * from players', function(err, result) {
                con.release();
                res.send(result);
            });
        });
    };

    this.getOne = function(id, res) {
        connection.acquire(function(err, con) {
            con.query('select * from players where id = ?', [id], function(err, result) {
                con.release();
                res.send(result);
            });
        });
    };

    this.delete = function(id, res) {
        connection.acquire(function(err, con) {
            con.query('delete from players where id = ?', [id], function(err, result) {
                con.release();
                if (err) {
                    res.send({status: 1, message: 'Failed to delete'});
                } else {
                    res.send({status: 0, message: 'Deleted successfully'});
                }
            });
        });
    };
}

module.exports = new Player();